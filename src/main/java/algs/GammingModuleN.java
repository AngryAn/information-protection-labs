package algs;

import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

public class GammingModuleN {
    static final Map<Character, Integer> alphabet = new HashMap<>();
    static final Map<Integer, Character> alphabet_ = new HashMap<>();
    static final int N = 39;

    // Свой алфавит, тк в данный в задании не соотвествует,
    // тому что выдаёт string.getBytes() (а он выдаёт по 2 байта на символ), слегка дополненный
    static {
        int i = 0;
        alphabet.put('А', i++);//0
        alphabet.put('Б', i++);
        alphabet.put('В', i++);
        alphabet.put('Г', i++);
        alphabet.put('Д', i++);
        alphabet.put('Е', i++);
        alphabet.put('Ё', i++);
        alphabet.put('Ж', i++);
        alphabet.put('З', i++);
        alphabet.put('И', i++);
        alphabet.put('Й', i++);
        alphabet.put('К', i++);
        alphabet.put('Л', i++);
        alphabet.put('М', i++);
        alphabet.put('Н', i++);
        alphabet.put('О', i++);
        alphabet.put('П', i++);
        alphabet.put('Р', i++);
        alphabet.put('С', i++);
        alphabet.put('Т', i++);
        alphabet.put('У', i++);
        alphabet.put('Ф', i++);
        alphabet.put('Х', i++);
        alphabet.put('Ц', i++);
        alphabet.put('Ч', i++);
        alphabet.put('Ш', i++);
        alphabet.put('Щ', i++);
        alphabet.put('Ъ', i++);
        alphabet.put('Ы', i++);
        alphabet.put('Ь', i++);
        alphabet.put('Э', i++);
        alphabet.put('Ю', i++);
        alphabet.put('Я', i++);//31
        alphabet.put('_', i++);
        alphabet.put('?', i++);
        alphabet.put('.', i++);
        alphabet.put(',', i++);
        alphabet.put('-', i++);
        alphabet.put(' ', i++);//37
        for (Map.Entry<Character, Integer> entry : alphabet.entrySet())
            alphabet_.put(entry.getValue(), entry.getKey());
    }

    public static void main(String[] args) {
        Formatter f = new Formatter(new StringBuffer());
        String key = "Ярославское шоссе".toUpperCase();
        String mess = "Ольга лучше всех".toUpperCase();
        f.format("Ключ:      %-30s:%3d%n" +
                "Сообщение: %-30s:%3d%n", key, key.length(), mess, mess.length());

        // Шифрование
        Character[] res = makeGammingNEncrypt(key, mess);
        // Дешифрование
        String stringDecrypt = makeGammingNDecrypt(key, res);

        Formatter byteFormatter = new Formatter(new StringBuffer());
        for (int i = 0; i < res.length; i++) {
            byteFormatter.format("%c", res[i]);
        }
        f.format("Зашифрованно байт: %d%n" +
                "Зашифрованное сообщение:  %s%n" +
                "Расшифрованное сообщение: %s%n", res.length, byteFormatter, stringDecrypt);
        System.out.println(f);
    }

    /**
     * Подготовка данных и их шифрование
     *
     * @param key  Ключ
     * @param mess Данные
     * @return Массив байт с зашифрованной строкой
     */
    static Character[] makeGammingNEncrypt(String key, String mess) {

        // Вырвнивание размера строки и ключа
        String keyOriginal = key;
        if (key.length() > mess.length())
            key = stringExtension(key, mess.length());
        else
            while (key.length() > mess.length())
                key += keyOriginal;

        // Шифрование
        return encrypt(key, mess, alphabet);
    }

    /**
     * Добирает или урезает строку до wordLength
     *
     * @param message обрабатываемая строка
     * @param wordLength необходимый размер получаемой строки (Длина публичного ключа)
     * @return
     */
    private static String stringExtension(String message, int wordLength) {
        // Длина шифруемого слова
        int messageLength = message.length();
        //make the secret word length equals to public word

        // Делает длину шифруемого слова соотвествующей длине публичного ключа
        int diff = wordLength - messageLength;

        // Шифруемое слово добирается или урезается до длины публичного ключа
        // Если длина публичного ключа > длины шифруемого слова
        if (diff > 0) {
            // Колличество раундов (roundsNum) = (длина публичного ключа - длина шифруемого слова) / длина шифруемого слова + 1
            int roundsNum = diff / messageLength + 1;
            for (int i = 0; i < roundsNum; i++) {
                message = message + message.substring(0, Math.min(diff, messageLength));
                diff = diff - Math.min(diff, messageLength);
            }
        } else {
            message = message.substring(0, messageLength + diff);
        }

        return message;
    }

    /**
     * Подготавливает алфавит и Дешифрует
     *
     * @param key  Ключ
     * @param mess Данные
     * @return Расшифрованная строка
     */
    static String makeGammingNDecrypt(String key, Character[] mess) {

        // Вырвнивание размера строки и ключа
        String keyOriginal = key;
        if (key.length() > mess.length)
            key = stringExtension(key, mess.length);
        else
            while (key.length() > mess.length)
                key += keyOriginal;

        // Дешифрование
        Character[] res = decrypt(key, mess, alphabet);
        Formatter f = new Formatter(new StringBuffer());
        for (int i = 0; i < res.length; i++) {
            f.format("%c", res[i]);
        }
        return f.toString();
    }

    /**
     * Шифрование
     *
     * @param key      Ключ
     * @param data     Данные
     * @param alphabet Алфавит
     * @return Массив байт с зашифрованной строкой
     */
    private static Character[] encrypt(String key, String data, Map<Character, Integer> alphabet) {
        // Длина шифруемого слова
        int wordLength = key.length();
        // Массив с результатом шифрования
        Character[] result = new Character[wordLength];
        // Шифруем каждый символ
        for (int i = 0; i < wordLength; i++) {
            result[i] = alphabet_.get((alphabet.get(key.charAt(i)) + alphabet.get(data.charAt(i))) % N);
        }
        return result;
    }

    /**
     * Дешифрование
     *
     * @param key        Ключ
     * @param message Данные
     * @param alphabet   Алфавит
     * @return Расшифрованная строка
     */
    private static Character[] decrypt(String key, Character[] message, Map<Character, Integer> alphabet) {
        // Длина шифруемого слова
        int wordLength = message.length;
        // Массив с результатом дешифрования
        Character[] result = new Character[wordLength];
        // Дешифруем каждый символ
        for (int i = 0; i < wordLength; i++) {
            result[i] = alphabet_.get((alphabet.get(message[i]) + N - alphabet.get(key.charAt(i))) % N);
        }
        return result;
    }
}
