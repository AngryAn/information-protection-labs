package algs;

import java.math.BigInteger;
import java.util.*;


// https://www.youtube.com/watch?v=vooHjWxmcIE
public class RSA {
    // Алфавит который позволяет из символа получить число
    static final Map<Character, Integer> alphabet = new HashMap<>();
    // Алфавит который позволяет из числа получить символ
    static final Map<Integer, Character> alphabet_ = new HashMap<>();

    static {
        int i = 0;
        alphabet.put('А', i++);//0
        alphabet.put('Б', i++);
        alphabet.put('В', i++);
        alphabet.put('Г', i++);
        alphabet.put('Д', i++);
        alphabet.put('Е', i++);
        alphabet.put('Ё', i++);
        alphabet.put('Ж', i++);
        alphabet.put('З', i++);
        alphabet.put('И', i++);
        alphabet.put('Й', i++);
        alphabet.put('К', i++);
        alphabet.put('Л', i++);
        alphabet.put('М', i++);
        alphabet.put('Н', i++);
        alphabet.put('О', i++);
        alphabet.put('П', i++);
        alphabet.put('Р', i++);
        alphabet.put('С', i++);
        alphabet.put('Т', i++);
        alphabet.put('У', i++);
        alphabet.put('Ф', i++);
        alphabet.put('Х', i++);
        alphabet.put('Ц', i++);
        alphabet.put('Ч', i++);
        alphabet.put('Ш', i++);
        alphabet.put('Щ', i++);
        alphabet.put('Ъ', i++);
        alphabet.put('Ы', i++);
        alphabet.put('Ь', i++);
        alphabet.put('Э', i++);
        alphabet.put('Ю', i++);
        alphabet.put('Я', i++);//31
        alphabet.put('_', i++);
        alphabet.put('?', i++);
        alphabet.put('.', i++);
        alphabet.put(',', i++);
        alphabet.put('-', i++);
        alphabet.put(' ', i++);//37
        for (Map.Entry<Character, Integer> entry : alphabet.entrySet())
            alphabet_.put(entry.getValue(), entry.getKey());
    }

    public static void main(String[] args) {
        long p = 101;
        long q = 103;
        if (IsTheNumberSimple(p) && IsTheNumberSimple(q)) {
            String data = "Ольга лучше всех".toUpperCase();
            long n = p * q;
            long m = (p - 1) * (q - 1);

            // d используется для дешифрования
            long d = Calculate_d(m);
            // e используется для шифрования
            long e_ = Calculate_e(d, m);

            // Шифрование
            String[] result = RSA_Endoce(data, e_, n);
            // Дешифрование
            String decoded = RSA_Decode(result, d, n);

            Formatter f = new Formatter(new StringBuffer());
            for (String part : result) {
                f.format("%s:", part);
            }
            System.out.println("Данные для шифрования: " + data);
            System.out.printf("P: %d, Q: %d, N: %d, M:%d, D: %d, E:%d%n", p, q, n, m, d, e_);
            System.out.println(f);
            System.out.println("Расшифрованные данные: " + decoded);
        } else
            System.out.println("p или q-непростыечисла!");
    }

    /**
     * Проверка, на то простое ли число
     *
     * @param n Число которое проверяем
     * @return True - простое, False - сложное
     */
    private static boolean IsTheNumberSimple(long n) {
        if (n < 2) return false;
        if (n == 2) return true;
        // Проверяем все делители, если нашёлся хотя бы один, значит число не простое
        for (long i = 2; i < n; i++) if (n % i == 0) return false;
        return true;
    }

    /**
     * Вычисление параметра d. d должно быть взаимно простым с m, используется для дешифрования
     *
     * @param m Число с которого начинаем поиск
     * @return d
     */
    private static long Calculate_d(long m) {
        long d = m - 1;
        for (long i = 2; i <= m; i++)
            if ((m % i == 0) && (d % i == 0)) //если имеют общие делители
            {
                d--;
                i = 1;
            }
        return d;
    }

    /**
     * Вычисление параметра e, который используется для шифрования
     *
     * @param d Взаимно простое к m число
     * @param m Параметр M
     * @return Параметр e
     */
    private static long Calculate_e(long d, long m) {
        long e = 10;
        while (true) {
            if ((e * d) % m == 1) break;
            else e++;
        }
        return e;
    }

    /**
     * Шифрование
     *
     * @param s Сообщение
     * @param e Параметр e
     * @param n Произведение простых чисел
     * @return Строки с числами полученными в результате шифрования
     */
    private static String[] RSA_Endoce(String s, long e, long n) {
        String[] result = new String[s.length()];
        BigInteger bi;
        for (int i = 0; i < s.length(); i++) {
            int character_index = alphabet.get(s.charAt(i));
            bi = BigInteger.valueOf(character_index);
            bi = bi.pow((int) e); // Возводим в степень
            BigInteger n_ = BigInteger.valueOf((int) n);
            bi = bi.mod(n_); // Бедём остаток от деления полученного числа на n
            result[i] = bi.toString();
        }
        return result;
    }

    /**
     * Дешифрование
     *
     * @param input Строки с числами полученными в результате шифрования
     * @param d     Параметр d
     * @param n     Произведение простых чисел
     * @return
     */
    private static String RSA_Decode(String input[], long d, long n) {
        String result = "";
        BigInteger bi;
        for (String item : input) {
            bi = BigInteger.valueOf(Long.valueOf(item));
            bi = bi.pow((int) d); // Возводим в степень
            BigInteger n_ = BigInteger.valueOf(n);
            bi = bi.mod(n_); // Бедём остаток от деления полученного числа на n
            int character_index = bi.intValue();
            result += alphabet_.get(character_index);
        }
        return result;
    }
}
