package algs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class PlayfairClipher extends Algorithm {
    char[] al = null;
    char[][] kyb = null;
    char[] add = {'-', '+', '/'};
    char def = 'я';

    @Override
    protected void keyProcess() {
        List chars = new ArrayList<Character>();
        for (Character ch : key.toCharArray())
            if (!chars.contains(ch))
                chars.add(ch);
        for (Character ch : alphabet)
            if (!chars.contains(ch))
                chars.add(ch);
        al = ((String) chars.stream().map(c -> c + "")
                .collect(Collectors.joining(""))).toCharArray();
        kyb = new char[6][6];

        int counter = 0;
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (counter < al.length)
                    kyb[m][n] = al[counter++];
                else
                    kyb[m][n] = add[counter++ - al.length];
    }

    private String change(String ch) {

        switch (type(ch)) {
            case 1: // в одной строке
                return "" + moveRight(ch.charAt(0)) + moveRight(ch.charAt(1));
            case 2: // в  одном столбце
                return "" + moveDown(ch.charAt(0)) + moveDown(ch.charAt(1));
            case 3: // в разных столбцах и разных строках
                return moveMirror(ch);
            default:
                throw new RuntimeException("Я так не умею");
        }
    }

    private String dechange(String ch) {

        switch (type(ch)) {
            case 1: // в одной строке
                return "" + moveLeft(ch.charAt(0)) + moveLeft(ch.charAt(1));
            case 2: // в  одном столбце
                return "" + moveUP(ch.charAt(0)) + moveUP(ch.charAt(1));
            case 3: // в разных столбцах и разных строках
                return moveMirror(ch);
            default:
                throw new RuntimeException("Я так не умею");
        }
    }

    private int type(String ch) {
        char ch1 = ch.charAt(0), ch2 = ch.charAt(1);
        int row1 = findRow(ch1), row2 = findRow(ch2);
        int column1 = findColumn(ch1), column2 = findColumn(ch2);
        if (row1 == row2)
            return 1;
        if (column1 == column2)
            return 2;
        return 3;
    }

    private String moveMirror(String ch) {
        char ch1 = ch.charAt(0), ch2 = ch.charAt(1);
        int str1 = findRow(ch1), str2 = findRow(ch2);
        String out = "";
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (kyb[m][n] == ch1) {
                    out += kyb[str2][n];
                } else if (kyb[m][n] == ch2) {
                    out += kyb[str1][n];
                }
        return out;
    }

    private int findRow(char ch) {
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (kyb[m][n] == ch) {
                    return m;
                }
        return 0;
    }

    private int findColumn(char ch) {
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (kyb[m][n] == ch) {
                    return n;
                }
        return 0;
    }

    private char moveLeft(char ch) {
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (kyb[m][n] == ch) {
                    return kyb[m][(kyb[m].length + n - 1) % kyb[m].length];
                }
        return '`';
    }

    private char moveRight(char ch) {
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (kyb[m][n] == ch) {
                    return kyb[m][(kyb[m].length + n + 1) % kyb[m].length];
                }
        return '`';
    }

    private char moveDown(char ch) {
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (kyb[m][n] == ch) {
                    return kyb[(m + 1) % kyb.length][n];
                }
        return '`';
    }

    private char moveUP(char ch) {
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (kyb[m][n] == ch) {
                    return kyb[(kyb.length + m - 1) % kyb.length][n];
                }
        return '`';
    }

    @Override
    public String encrypt(String data) {
        List<String> st = new ArrayList<>();
        for (int i = 0; i < data.length(); i++) {
            //Тут вот какая мысль: Берём символ, если он не последний и если следующий за ним не такой же
            if (i + 1 < data.length() && data.charAt(i) != data.charAt(i + 1))
                st.add("" + data.charAt(i) + data.charAt(++i));
            else
                st.add("" + data.charAt(i) + def);
        }
        System.out.println(st);
        return st.stream().map(c -> change(c)).map(c -> c + "").collect(Collectors.joining(" "));
    }

    @Override
    public String decrypt(String data) {
        List<String> st = new ArrayList<>();
        for (int i = 0; i < data.length(); i++) {
            //Тут вот какая мысль: Берём символ, если он не последний и если следующий за ним не такой же
            if (i + 1 < data.length() && data.charAt(i) != data.charAt(i + 1))
                st.add("" + data.charAt(i) + data.charAt(++i));
            else
                st.add("" + data.charAt(i) + def);
        }
        System.out.println(st);
        return st.stream().map(c -> dechange(c)).map(c -> c + "").collect(Collectors.joining(" "));
    }
}
