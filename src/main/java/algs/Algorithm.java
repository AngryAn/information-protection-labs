package algs;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public abstract class Algorithm {
    protected static final char[] alphabet = {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'};
    private static final Scanner sc = new Scanner(System.in);
    private static final Map<Character, Integer> charToInt = charToInt(alphabet);
    protected String key;

    public Algorithm() {
    }

    static Map charToInt(char[] alphabet) {
        Map m = new HashMap();
        int i = 0;
        for (Character c : alphabet)
            m.put(c, i++);
        return m;
    }

    public void start() {
        int label_size = 13;
        enterKey();
        keyProcess();
        System.out.print("Введите строку или Ctrl+D для выхода:");
        while (sc.hasNextLine()) {
            String in = sc.nextLine();
            in = in.toLowerCase();
            System.out.print(String.format("%" + label_size + "s: %-9s%n", "Ключ", key));
            System.out.print(String.format("%" + label_size + "s: %-9s%n", "Данные", in));
            System.out.print(String.format("%" + label_size + "s: %-9s%n", "Зашифровано", encrypt(in)));
            System.out.print(String.format("%" + label_size + "s: %-9s%n", "Расшифрованно", decrypt(in)));
            System.out.print("\n\n");
            System.out.print("Введите строку или Ctrl+D для выхода:");
        }
        sc.close();
    }

    public void enterKey() {
        System.out.print("Введите ключ для шифрования:");
        while (key == null)
            key = sc.nextLine();
        key = key.toLowerCase();
    }

    protected void keyProcess() {
    }

    public abstract String encrypt(String data);

    public abstract String decrypt(String data);

    protected char getCharByNumber(int i, char[] alphabet) {
        if (i >= 0)
            return alphabet[(i % alphabet.length)];
        else
            return alphabet[((alphabet.length - i) % alphabet.length)];
    }

    protected Integer getNumberByChar(Character c, char[] alphabet) {
        Map<Character, Integer> custom = charToInt(alphabet);
        return custom.get(c);
    }

    protected char getCharByNumber(int i) {
        return getCharByNumber(i, alphabet);
    }

    protected Integer getNumberByChar(Character c) {
        return charToInt.get(c);
    }
}
