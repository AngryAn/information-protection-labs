package algs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LozungovyyCipher extends Algorithm{
    char[] al = null;

    @Override
    protected void keyProcess() {
        List chars = new ArrayList<Character>();
        for (Character ch : key.toCharArray())
            if (!chars.contains(ch))
                chars.add(ch);
        for (Character ch : alphabet)
            if (!chars.contains(ch))
                chars.add(ch);
        al = ((String)chars.stream().map(c -> c + "")
                .collect(Collectors.joining(""))).toCharArray();
    }

    @Override
    public String encrypt(String data) {
        return data.chars()
                .mapToObj(i -> getNumberByChar((char) i))// Получаем номера букв в алфавите
                .map(i -> getCharByNumber(i, al))
                .map(c -> c + "")
                .collect(Collectors.joining(""));
    }

    @Override
    public String decrypt(String data) {
        return data.chars()
                .mapToObj(i -> getNumberByChar((char) i, al))// Получаем номера букв в алфавите
                .map(i -> getCharByNumber(i))
                .map(c -> c + "")
                .collect(Collectors.joining(""));
    }
}
