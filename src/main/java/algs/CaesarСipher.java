package algs;

import java.util.stream.Collectors;

public class CaesarСipher extends Algorithm {
    @Override
    public String encrypt(String data) {
        return data.chars()
                .mapToObj(i -> getNumberByChar((char) i))// Получаем номера букв в алфавите
                .map(i -> getCharByNumber(i + Integer.parseInt(key)))// Собственно шифрование
                .map(c -> c + "")
                .collect(Collectors.joining(""));
    }

    @Override
    public String decrypt(String data) {
        return data.chars()
                .mapToObj(i -> getNumberByChar((char) i))// Получаем номера букв в алфавите
                .map(i -> getCharByNumber(i - Integer.parseInt(key)))// Дешифрование
                .map(c -> c + "")
                .collect(Collectors.joining(""));
    }
}
