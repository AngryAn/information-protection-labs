package algs;

import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class GammingModule2 {
    static final Map<Character, Byte> alphabet = new HashMap<>();

    // Свой алфавит, тк в данный в задании не соотвествует,
    // тому что выдаёт string.getBytes() (а он выдаёт по 2 байта на символ), слегка дополненный
    static {
        alphabet.put('*', (byte) 0b00000000);
        alphabet.put('А', (byte) 0b10000000);
        alphabet.put('Б', (byte) 0b10000001);
        alphabet.put('В', (byte) 0b10000010);
        alphabet.put('Г', (byte) 0b10000011);
        alphabet.put('Д', (byte) 0b10000100);
        alphabet.put('Е', (byte) 0b10000101);
        alphabet.put('Ё', (byte) 0b10000101);
        alphabet.put('Ж', (byte) 0b10000110);
        alphabet.put('З', (byte) 0b10000111);
        alphabet.put('И', (byte) 0b10001000);
        alphabet.put('Й', (byte) 0b10001001);
        alphabet.put('К', (byte) 0b10001010);
        alphabet.put('Л', (byte) 0b10001011);
        alphabet.put('М', (byte) 0b10001100);
        alphabet.put('Н', (byte) 0b10001101);
        alphabet.put('О', (byte) 0b10001110);
        alphabet.put('П', (byte) 0b10001111);
        alphabet.put('Р', (byte) 0b10010000);
        alphabet.put('С', (byte) 0b10010001);
        alphabet.put('Т', (byte) 0b10010010);
        alphabet.put('У', (byte) 0b10010011);
        alphabet.put('Ф', (byte) 0b10010100);
        alphabet.put('Х', (byte) 0b10010101);
        alphabet.put('Ц', (byte) 0b10010110);
        alphabet.put('Ч', (byte) 0b10010111);
        alphabet.put('Ш', (byte) 0b10011000);
        alphabet.put('Щ', (byte) 0b10011001);
        alphabet.put('Ъ', (byte) 0b10011010);
        alphabet.put('Ы', (byte) 0b10011011);
        alphabet.put('Ь', (byte) 0b10011100);
        alphabet.put('Э', (byte) 0b10011101);
        alphabet.put('Ю', (byte) 0b10011110);
        alphabet.put('Я', (byte) 0b10011111);
        alphabet.put('_', (byte) 0b10100000);
        alphabet.put('?', (byte) 0b10100010);
        alphabet.put('.', (byte) 0b10100011);
        alphabet.put(',', (byte) 0b10100100);
        alphabet.put('-', (byte) 0b10100101);
        alphabet.put(' ', (byte) 0b10100110);
    }


    public static void main(String[] args) {
        Formatter f = new Formatter(new StringBuffer());

        String publicWorld = "Трансляторбинарногокода".toUpperCase();
        String privateWorld = "Мотивациянауспех".toUpperCase();

        f.format("Public world:  %-40s:%3d%nPrivate world: %-40s:%3d", publicWorld, publicWorld.length(), privateWorld, privateWorld.length());

        // Шифрование
        Byte[] res = GammingModule2.makeGamming2Encrypt(publicWorld, privateWorld);
        // Дешифрование
        String stringDecrypt = GammingModule2.makeGamming2Decrypt(publicWorld, res);

        Formatter byteFormatter = new Formatter(new StringBuffer());
        for (int i = 0; i < res.length; i++) {
            byteFormatter.format("%02x:", res[i]);
        }
        f.format("Bytes: %d%nEncrypt Data: %s%nDecrypt Data: %s%n", res.length, byteFormatter, stringDecrypt);
        System.out.println(f);
    }

    /**
     * Подготовка данных и их шифрование
     *
     * @param gamma Ключ
     * @param mess  Данные
     * @return Массив байт с зашифрованной строкой
     */
    static Byte[] makeGamming2Encrypt(String gamma, String mess) {

        // Длина сообщения
        int messLength = mess.length();

        // Выравниваем гамму до размера сообщения
        if (gamma.length() != mess.length())
            gamma = stringExtension(gamma, messLength);

        // Шифрование
        return encrypt(gamma, mess, alphabet);
    }

    /**
     * Подготавливает алфавит и Дешифрует
     *
     * @param gamma Ключ
     * @param mess  Данные
     * @return Расшифрованная строка
     */
    static String makeGamming2Decrypt(String gamma, Byte[] mess) {
        Map<Byte, Character> alphabet_ = new HashMap<>();
        for (Map.Entry<Character, Byte> entry : alphabet.entrySet())
            alphabet_.put(entry.getValue(), entry.getKey());

        // Выравниваем гамму до размера сообщения
        if (gamma.length() != mess.length)
            gamma = stringExtension(gamma, mess.length);

        // Дешифрование
        return Arrays.stream(decrypt(gamma, mess, alphabet)).map(b -> alphabet_.get(b).toString()).collect(Collectors.joining("", "", ""));
    }

    /**
     * Добирает или урезает строку до wordLength
     *
     * @param mess       обрабатываемая строка
     * @param wordLength необходимый размер получаемой строки (Длина публичного ключа)
     * @return
     */
    private static String stringExtension(String mess, int wordLength) {
        // Длина шифруемого слова
        int secretWordLength = mess.length();
        //make the secret word length equals to public word

        // Делает длину шифруемого слова соотвествующей длине публичного ключа
        int diff = wordLength - secretWordLength;

        // Cлово добирается или урезается до необходимой длины
        // Если длина публичного ключа > длины шифруемого слова
        if (diff > 0) {
            // Колличество раундов (roundsNum) = (длина публичного ключа - длина шифруемого слова) / длина шифруемого слова + 1
            int roundsNum = diff / secretWordLength + 1;
            for (int i = 0; i < roundsNum; i++) {
                mess = mess + mess.substring(0, Math.min(diff, secretWordLength));
                diff = diff - Math.min(diff, secretWordLength);
            }
        } else {
            mess = mess.substring(0, secretWordLength + diff);
        }

        return mess;
    }

    /**
     * Шифрование
     *
     * @param publicWord Ключ
     * @param secretWord Данные
     * @param alphabet   Алфавит
     * @return Массив байт с зашифрованной строкой
     */
    private static Byte[] encrypt(String publicWord, String secretWord, Map<Character, Byte> alphabet) {
        // Если длины слов не совпадают, выкидываем ошибку
        if (publicWord.length() != secretWord.length())
            throw new RuntimeException("Длины слов не совпадают!");
        // Длина шифруемого слова
        int wordLength = publicWord.length();
        // Массив с результатом шифрования
        Byte[] result = new Byte[wordLength];

        // Шифруем каждый символ
        for (int i = 0; i < wordLength; i++) {
            result[i] = (byte) (alphabet.get(publicWord.charAt(i)) ^ alphabet.get(secretWord.charAt(i)));
        }
        return result;
    }

    /**
     * Дешифрование
     *
     * @param publicWord Ключ
     * @param secretWord Данные
     * @param alphabet   Алфавит
     * @return Расшифрованная строка
     */
    private static Byte[] decrypt(String publicWord, Byte[] secretWord, Map<Character, Byte> alphabet) {
        // Если длины слов не совпадают, выкидываем ошибку
        if (publicWord.length() != secretWord.length)
            throw new RuntimeException("Длины слов не совпадают!");
        // Длина шифруемого слова
        int wordLength = publicWord.length();
        // Массив с результатом дешифрования
        Byte[] result = new Byte[wordLength];

        // Дешифруем каждый символ
        for (int i = 0; i < wordLength; i++) {
            result[i] = (byte) (alphabet.get(publicWord.charAt(i)) ^ secretWord[i]);
        }
        return result;
    }
}

