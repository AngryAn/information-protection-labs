package algs;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.Formatter;
import java.util.HashMap;
import java.util.Map;

public class GOST28147 {
    static final Map<Character, Byte> alphabet = new HashMap<>();

    /**
     * Таблица замен (S-таблица)
     * Каждая строка должна содержать от 0x0 до 0xF, без повторений
     */
    static final Byte[][] Tab_Z = new Byte[][]{
            {0xF, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0x0}, // Подключ 1 (16 байт)
            {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF},
            {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF},
            {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF},
            {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF},
            {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF},
            {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF},
            {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF}
    };

    // Ключ (256 bit = 32 * 8 bit)
    static final int[] key = {
            0b00000000000000000000000000000000, // k0 32-bit
            0b00000000000000000000000000000000,
            0b00000000000000010000000000000000,
            0b00000000000000000000000000000000,
            0b00000000000000000000000000000000,
            0b00000000000000000000000000000000,
            0b00000000000000000000000000000000,
            0b00000000000000000000000000000000 // k7
    };

    // Используется для перевода Byte -> Long и Long -> Byte
    private static ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);

    // Свой алфавит, тк в данный в задании не соотвествует,
    // тому что выдаёт string.getBytes() (а он выдаёт по 2 байта на символ), слегка дополненный
    static {
        alphabet.put('*', (byte) 0b00000000);
        alphabet.put('А', (byte) 0b10000000);
        alphabet.put('Б', (byte) 0b10000001);
        alphabet.put('В', (byte) 0b10000010);
        alphabet.put('Г', (byte) 0b10000011);
        alphabet.put('Д', (byte) 0b10000100);
        alphabet.put('Е', (byte) 0b10000101);
        alphabet.put('Ё', (byte) 0b10000101);
        alphabet.put('Ж', (byte) 0b10000110);
        alphabet.put('З', (byte) 0b10000111);
        alphabet.put('И', (byte) 0b10001000);
        alphabet.put('Й', (byte) 0b10001001);
        alphabet.put('К', (byte) 0b10001010);
        alphabet.put('Л', (byte) 0b10001011);
        alphabet.put('М', (byte) 0b10001100);
        alphabet.put('Н', (byte) 0b10001101);
        alphabet.put('О', (byte) 0b10001110);
        alphabet.put('П', (byte) 0b10001111);
        alphabet.put('Р', (byte) 0b10010000);
        alphabet.put('С', (byte) 0b10010001);
        alphabet.put('Т', (byte) 0b10010010);
        alphabet.put('У', (byte) 0b10010011);
        alphabet.put('Ф', (byte) 0b10010100);
        alphabet.put('Х', (byte) 0b10010101);
        alphabet.put('Ц', (byte) 0b10010110);
        alphabet.put('Ч', (byte) 0b10010111);
        alphabet.put('Ш', (byte) 0b10011000);
        alphabet.put('Щ', (byte) 0b10011001);
        alphabet.put('Ъ', (byte) 0b10011010);
        alphabet.put('Ы', (byte) 0b10011011);
        alphabet.put('Ь', (byte) 0b10011100);
        alphabet.put('Э', (byte) 0b10011101);
        alphabet.put('Ю', (byte) 0b10011110);
        alphabet.put('Я', (byte) 0b10011111);
        alphabet.put('_', (byte) 0b10100000);
        alphabet.put('?', (byte) 0b10100010);
        alphabet.put('.', (byte) 0b10100011);
        alphabet.put(',', (byte) 0b10100100);
        alphabet.put('-', (byte) 0b10100101);
        alphabet.put(' ', (byte) 0b10100110);
    }


    public static void main(String[] args) {
        Formatter f = new Formatter(new StringBuffer());
        String privateWorld = "Мотивация на успех".toUpperCase();
        f.format("Private world: %-40s:%3d", privateWorld, privateWorld.length());

        // Шифрование
        byte[] res = encryption(privateWorld);
        // Дешифрование
        String stringDecrypt = GOST28147.decryption(res);

        Formatter byteFormatter = new Formatter(new StringBuffer());
        for (int i = 0; i < res.length; i++) {
            byteFormatter.format("%02x:", res[i]);
        }
        f.format("Bytes: %d%nEncrypt Data: %s%nDecrypt Data: %s%n", res.length, byteFormatter, stringDecrypt);
        System.out.println(f);
    }

    /**
     * Шифрование строки
     * Автоматически переводит строку в набор байт, который за тем шифруется.
     * Строка может быть дополненна '_' для кратности её длины 8.
     *
     * @param mess строка которая будет зашифрованна
     * @return byte[] результат шифрования, один сивол = 1 байт
     */
    public static byte[] encryption(String mess) {
        byte[] bytes = stringToByte(mess);
        long[] out = byteArrayToLongArray(bytes);
        out = encryption(out);
        bytes = longArrayToByteArray(out);
        return bytes;
    }

    /**
     * Дешифрование строки
     * Дешифрует набор байт, который должен быть кратен 8 байтам
     *
     * @param data данные которые необходимо расшифровать
     * @return String дешифрованная строка
     */
    public static String decryption(byte[] data) {
        long[] out = byteArrayToLongArray(data);
        out = decryption(out);
        byte[] bytes = longArrayToByteArray(out);
        String mess = byteToString(bytes);
        return mess;
    }

    /**
     * Переводит набор байт в строку, согласно alphabet.
     * Неизвестные символы будут замененны на ?
     *
     * @param data byte[] набор символов
     * @return String полученная строка
     */
    public static String byteToString(byte[] data) {
        // Первым делом формируем алфавит по которому будет удобно искать (Map<Код_символа, Сам_Символ>)
        Map<Byte, Character> alphabet_ = new HashMap<>();
        StringBuffer sb = new StringBuffer(data.length);
        for (Map.Entry<Character, Byte> entry : alphabet.entrySet())
            alphabet_.put(entry.getValue(), entry.getKey());
        // Проходимся по всем полученным байтам и заменяем их на символы
        for (int i = 0; i < data.length; i++) {
            Character ch = alphabet_.get(data[i]);
            sb.append(ch == null ? '?' : ch);
        }
        return sb.toString();
    }

    /**
     * Переводит строку в набор байт согласно alphabet.
     * Неизвестные символы будут замененны на '?' или 0x0
     *
     * @param mess Текст который преобразуется в байты
     * @return byte[] байтовый эквивалент mess
     */
    public static byte[] stringToByte(String mess) {
        byte[] bytes;
        if (mess.length() % 8 != 0) {
            bytes = new byte[mess.length() + (8 - mess.length() % 8)];
            int diff = 8 - mess.length() % 8;
            for (int i = 0; i < diff; i++) {
                mess += "_";
            }
        } else
            bytes = new byte[mess.length()];
        for (int i = 0; i < mess.length(); i++) {
            Byte ch = alphabet.get(mess.charAt(i));
            // Проверяем что в нашем алфавите есть необходимый символ, иначе заменяем его кодом символа '?',
            // если и этого не получилось, то заменяем его на 8 нулевых bit (00000000)
            bytes[i] = ch == null ? (alphabet.get('?') == null ? 0x0 : alphabet.get('?')) : ch;
        }
        return bytes;
    }

    /**
     * Переводит масив байт в массивы по 8 байт (64 бит, в java тип long как раз 64 бита)
     *
     * @param data byte[] массив байт
     * @return long[] массив по 8 байт
     */
    public static long[] byteArrayToLongArray(byte[] data) {
        long[] out = new long[data.length / 8];
        for (int i = 0; i < data.length / 8; i++) {
            buffer.put(Arrays.copyOfRange(data, i * 8, (i + 1) * 8));
            buffer.flip();
            out[i] = buffer.getLong();
            buffer.clear();
        }
        return out;
    }

    /**
     * Переводит масивы по 8 байт в массив байт (в java тип long как раз 64 бита)
     *
     * @param data long[] массив по 8 байт
     * @return byte[] массив байт
     */
    public static byte[] longArrayToByteArray(long[] data) {
        byte[] bytes = new byte[data.length * 8];
        for (int i = 0; i < data.length; i++) {
            buffer.putLong(0, data[i]);
            byte[] array = buffer.array();
            for (int j = 0; j < array.length; j++) {
                bytes[i * 8 + j] = array[j];
            }
            buffer.clear();
        }
        return bytes;
    }

    /**
     * Шифрование кусочнов по 64 бит
     *
     * @param data long[] массив с данными
     * @return long[] массив с зашифрованными данными
     */
    public static long[] encryption(long[] data) {
        long[] out = new long[data.length];
        int bit = 0;
        for (long bits : data) {
            for (int i = 0; i < 32; i++) {
                bits = round(bits, key[i % 8]);
                //System.out.println("Step %2d: %s".formatted(i, longTo64Bit(bits)));
            }
            out[bit++] = bits;
        }
        return out;
    }

    /**
     * Дешифрование кусочнов по 64 бит
     *
     * @param data long[] массив с данными
     * @return long[] массив с расшифрованными данными
     */
    public static long[] decryption(long[] data) {
        long[] out = new long[data.length];
        int bit = 0;
        for (long bits : data) {
            for (int i = 31; i >= 0; i--) {
                bits = deRound(bits, key[i % 8]);
                //System.out.println("Step %2d: %s".formatted(i, longTo64Bit(bits)));
            }
            out[bit++] = bits;
        }
        return out;
    }


    /**
     * Вычисление одного раунда шифрования.
     *
     * @param data 64 бита данных
     * @param key  32-х битный раундовый ключ
     * @return 64 бита подвергнутых 1 раунду шифрования
     */
    public static long round(long data, int key) {
        // Разделение на правую и левую часть
        // Беззнаковый сдвиг в право на 32
        int left_original = (int) (data >>> 32), left = left_original;
        int right_original = (int) data, right = right_original;

        right = right ^ key;
        right = replacementBlock(right); // Подстановка в S таблицу
        right = rotateLeft(right, 11); // Циклический сдвиг
        right = right ^ left;

        // Соедение двух кусочков по 32 бита в один размером 64,
        // для правильного сложениея сначала необходимо их расширить до 64 бит (& 0xffffffffL),
        // потом левая часть сдвигается в лево на 32 бита, а правая записывается в последние 32 бита
        return ((right_original & 0xffffffffL) << 32) | (right & 0xffffffffL);
    }

    /* Вычисление одного раунда дефрования.
     *
     * @param data 64 бита шашифрованных данных
     * @param key  32-х битный раундовый ключ
     * @return 64 бита подвергнутых 1 раунду дешифрования
     */
    public static long deRound(long data, int key) {
        // Разделение на правую и левую часть
        // Беззнаковый сдвиг в право на 32
        int left_original = (int) (data >>> 32), left = left_original;
        int right_original = (int) data, right = right_original;

        left = left ^ key;
        left = replacementBlock(left); // Подстановка в S таблицу
        left = rotateLeft(left, 11); // Циклический сдвиг
        left = left ^ right;

        // Соедение двух кусочков по 32 бита в один размером 64,
        // для правильного сложениея сначала необходимо их расширить до 64 бит (& 0xffffffffL),
        // потом левая часть сдвигается в лево на 32 бита, а правая записывается в последние 32 бита
        return ((left & 0xffffffffL) << 32) | (left_original & 0xffffffffL);
    }

    /**
     * Подстановка в S блок
     *
     * @param data int 32-bit до перестановки
     * @return int 32-bit после перестановки
     */
    public static int replacementBlock(int data) {
        String bit32 = intTo32Bit(data);
        byte bytes[] = new byte[8];
        // Разбитие по 4 бита, каждый записывается в отдельный байт
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = Byte.parseByte(bit32.substring(i * 4, (i + 1) * 4), 2);
        }

        // Непосредственно подстановка
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = Tab_Z[i][bytes[i]];
        }

        // Собираем всё обратно в 32 bit
        int result = 0;
        for (int i = 0; i < bytes.length; i++) {
            // bytes.length * 4 - i : (8 - 1) * 4 = 28
            // Каждый следующий байт записывается со смещением в 4 бита
            result += bytes[i] << (bytes.length - (i + 1)) * 4;
        }
        return result;
    }


    /**
     * Обратная подстановка в S блок
     *
     * @param data int 32-bit до перестановки
     * @return int 32-bit после перестановки
     */
    public static int deReplacementBlock(int data) {
        String bit32 = intTo32Bit(data);
        byte bytes[] = new byte[8];
        // Разбитие по 4 бита, каждый записывается в отдельный байт
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = Byte.parseByte(bit32.substring(i * 4, (i + 1) * 4), 2);
        }
        // Непосредственно подстановка
        for (int i = 0; i < bytes.length; i++) {
            for (byte j = 0; j < Tab_Z[i].length; j++) {
                if (Tab_Z[i][j] == bytes[i]) {
                    bytes[i] = j;
                    break;
                }
            }
        }
        // Собираем всё обратно в 32 bit
        int result = 0;
        for (int i = 0; i < bytes.length; i++) {
            // bytes.length * 4 - i : (8 - 1) * 4 = 28
            // Каждый следующий байт записывается со смещением в 4 бита
            result += bytes[i] << (bytes.length - (i + 1)) * 4;
        }
        return result;
    }

    /**
     * Разбитие int на 32 бита
     *
     * @param data int
     * @return Строка состоящая из 0 и 1
     */
    public static String intTo32Bit(int data) {
        String patternString32 = "00000000000000000000000000000000";//32
        String bit32 = Integer.toUnsignedString(data, 2);
        bit32 = patternString32.substring(0, patternString32.length() - bit32.length()) + bit32;
        return bit32;
    }

    /**
     * Разбитие long на 62 бита
     *
     * @param data long
     * @return Строка состоящая из 0 и 1
     */
    public static String longTo64Bit(long data) {
        String patternString64 = "0000000000000000000000000000000000000000000000000000000000000000";//64
        String bit64 = Long.toUnsignedString(data, 2);
        bit64 = patternString64.substring(0, patternString64.length() - bit64.length()) + bit64;
        return bit64;
    }

    /**
     * Циклический сдвиг влево
     *
     * @param number Число в котором будет происходить сдвиг
     * @param shift  Сдвиг в битах
     * @return Число в котором сделан сдвиг влево
     */
    private static int rotateLeft(int number, int shift) {
        return ((number >> (32 - shift)) & ~((-1) << shift)) | number << 8;// цикло сдвиг влево на 8 бит
    }

    /**
     * Циклический сдвиг вправо
     *
     * @param number Число в котором будет происходить сдвиг
     * @param shift  Сдвиг в битах
     * @return Число в котором сделан сдвиг влево
     */
    private static int rotateRight(int number, int shift) {
        return number >> (shift) | ((number << (32 - shift)) & ~((-1) >> shift));// цикло сдвиг вправо на 8 бит
    }

}
