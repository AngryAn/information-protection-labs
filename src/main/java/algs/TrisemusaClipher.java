package algs;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TrisemusaClipher extends Algorithm{
    char[] al = null;
    char[][] kyb = null;

    @Override
    protected void keyProcess() {
        List chars = new ArrayList<Character>();
        for (Character ch : key.toCharArray())
            if (!chars.contains(ch))
                chars.add(ch);
        for (Character ch : alphabet)
            if (!chars.contains(ch))
                chars.add(ch);
        al = ((String) chars.stream().map(c -> c + "")
                .collect(Collectors.joining(""))).toCharArray();
        kyb = new char[6][6];
        char def = '-';
        int counter = 0;
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (counter < al.length)
                    kyb[m][n] = al[counter++];
                else
                    kyb[m][n] = def;
    }

    private char moveDown(char ch){
        for (int m = 0; m < kyb.length; m++)
            for (int n = 0; n < kyb[m].length; n++)
                if (kyb[m][n] == ch){
                    return kyb[(m+1)%kyb.length][n];
                }
        return '`';
    }

    private char moveUP(char ch){for (int m = 0; m < kyb.length; m++)
        for (int n = 0; n < kyb[m].length; n++)
            if (kyb[m][n] == ch){
                return kyb[(kyb.length+m-1)%kyb.length][n];
            }
        return '`';}

    @Override
    public String encrypt(String data) {
        return data.chars().mapToObj(c->moveDown((char) c)).map(c -> c + "").collect(Collectors.joining(""));
    }

    @Override
    public String decrypt(String data) {
        return data.chars().mapToObj(c->moveUP((char) c)).map(c -> c + "").collect(Collectors.joining(""));
    }
}
