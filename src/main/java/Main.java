import algs.Algorithm;
import algs.PlayfairClipher;

public class Main {
    public static void main(String[] args) {
        Algorithm al = new PlayfairClipher();
        al.start();
    }
}
