# Information Protection algs
Реализация алгоритмов шифрования в рамках лабораторных работ предмета 'Информационная безопасность' НИУ МГСУ, ИЭУИС IV.

Реализованные алгоритмы:
* [Алгоритм цезаря](src/main/java/algs/CaesarСipher.java) 
* [Лозунговый шифр](src/main/java/algs/LozungovyyCipher.java) 
* [Шифр Playfair(Плейфера)](src/main/java/algs/PlayfairClipher.java) 
* [ШифрТрисемуса(Тритемия)](src/main/java/algs/TrisemusaClipher.java) 
* [ГОСТ 28147-89](src/main/java/algs/GOST28147.java) 
* [Гаммирование по модулю 2](src/main/java/algs/GammingModule2.java) 
* [Гаммирование по модулю N](src/main/java/algs/GammingModuleN.java) 
* [RSA](src/main/java/algs/RSA.java) 


### Authors

* **Андрей Проказа** - *GOST28147, GammingModule2, GammingModuleN* - [GitLab](https://gitlab.com/AngryAn), [Telegram](https://t.me/AngryAn) 